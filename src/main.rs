mod arp;
mod cli_args;
mod interface;

fn main() -> Result<(), anyhow::Error> {
    let (
        operation,
        interface,
        arp_target_mac,
        arp_source_mac,
        arp_target_ip,
        arp_source_ip,
        ether_target_mac,
        ether_source_mac,
    ) = cli_args::parse_args()?;

    let _receiver = arp::get_mac_through_arp(
        operation,
        interface,
        arp_target_mac,
        arp_source_mac,
        arp_target_ip,
        arp_source_ip,
        ether_target_mac,
        ether_source_mac,
    )?;

    //possibly wait to receive a packet depending on operation
    // let arp_packet = arp::receive_packet(receiver)?;
    // println!("{}", arp_packet.get_sender_hw_addr());

    Ok(())
}
