use clap::{arg, Command, ArgMatches};
use pnet::{datalink::NetworkInterface, util::MacAddr, packet::arp::{ArpOperation, ArpOperations,},};
use std::{str::FromStr, net::Ipv4Addr};
use anyhow::anyhow;

pub fn parse_args() -> Result<
    (ArpOperation, NetworkInterface, MacAddr, MacAddr, Ipv4Addr, Ipv4Addr, MacAddr, MacAddr),
    anyhow::Error
> {
    let c = Command::new("arp3ggiator")
        .version("1.0")
        .about("Sends raw ARP packets. Can be used to send unsolicited ARP packets, including to poison")
        .subcommand_required(true)
        .subcommand(
            Command::new("query")
                .short_flag('Q')
                .long_flag("query")
                .about("Sends a query packet")
                .arg(arg!(-i --interface <interface> "Network device that will be used to send the packet")
                    .required(true)
                )
                .arg(arg!(-a --arp_target_mac <target_mac_address> "Ethernet address that will be set as destination in the ARP payload")
                    .default_value("00:00:00:00:00:00")
                )
                .arg(arg!(-b --arp_source_mac <source_mac_address> "Ethernet address that will be set as sender in the ARP payload")
                    .default_value("00:00:00:00:00:00")
                )
                .arg(arg!(-t --arp_target_ip <target_mac_address>  "IP address that will be set as destination in the ARP payload")
                    .default_value("0.0.0.0")
                )
                .arg(arg!(-s --arp_source_ip <target_mac_address>  "IP address that will be set as sender in the ARP payload")
                    .default_value("0.0.0.0")
                )
                .arg(arg!(-e --ether_target_mac <target_mac_address> "Ethernet address that will be set as destination in ethernet frame (layer 3)")
                    .default_value("00:00:00:00:00:00")
                )
                .arg(arg!(-f --ether_source_mac <target_mac_address>  "Ethernet address that will be set as source in ethernet frame (layer 3)")
                    .default_value("00:00:00:00:00:00")
                )
        )
        .subcommand(
            Command::new("answer")
                .short_flag('A')
                .long_flag("answer")
                .visible_alias("reply")
                .visible_alias("respond")
                .visible_alias("poison")
                .visible_alias("unsolicited-reply")
                .about("Sends a reply packet, whether solicited or not")
                .arg(arg!(-i --interface <interface> "Network device that will be used to send the packet")
                    .required(true)
                )
                .arg(arg!(-a --arp_target_mac <target_mac_address> "Ethernet address that will be set as destination in the ARP payload")
                    .default_value("00:00:00:00:00:00")
                )
                .arg(arg!(-b --arp_source_mac <source_mac_address> "Ethernet address that will be set as sender in the ARP payload")
                    .default_value("00:00:00:00:00:00")
                )
                .arg(arg!(-t --arp_target_ip <target_mac_address>  "IP address that will be set as destination in the ARP payload")
                    .default_value("0.0.0.0")
                )
                .arg(arg!(-s --arp_source_ip <target_mac_address>  "IP address that will be set as sender in the ARP payload")
                    .default_value("0.0.0.0")
                )
                .arg(arg!(-e --ether_target_mac <target_mac_address> "Ethernet address that will be set as destination in ethernet frame (layer 3)")
                    .default_value("00:00:00:00:00:00")
                )
                .arg(arg!(-f --ether_source_mac <target_mac_address>  "Ethernet address that will be set as source in ethernet frame (layer 3)")
                    .default_value("00:00:00:00:00:00")
                )
        );
    let matches = c.get_matches();

    Ok(
        match matches.subcommand() {
            Some(("query", args)) => {
                let operation = ArpOperations::Request;

                let (
                    interface,
                    arp_target_mac,
                    arp_source_mac,
                    arp_target_ip,
                    arp_source_ip,
                    ether_target_mac,
                    ether_source_mac,
                ) = parse_sub_args(args)?;

                (
                    operation,
                    interface,
                    arp_target_mac,
                    arp_source_mac,
                    arp_target_ip,
                    arp_source_ip,
                    ether_target_mac,
                    ether_source_mac,
                )
            },
            Some(("reply", args)) => {
                let operation = ArpOperations::Reply;

                let (
                    interface,
                    arp_target_mac,
                    arp_source_mac,
                    arp_target_ip,
                    arp_source_ip,
                    ether_target_mac,
                    ether_source_mac,
                ) = parse_sub_args(args)?;
           
                (
                    operation,
                    interface,
                    arp_target_mac,
                    arp_source_mac,
                    arp_target_ip,
                    arp_source_ip,
                    ether_target_mac,
                    ether_source_mac,
                )
            },
            _ => {Err(anyhow!("Clap failed to validate args - contact developer"))?},
        }
    )
}

fn parse_sub_args(
    args: &ArgMatches
) -> Result<
    (NetworkInterface, MacAddr, MacAddr, Ipv4Addr, Ipv4Addr, MacAddr, MacAddr), 
    anyhow::Error
> {
    let interface = args.get_one::<String>("interface").unwrap();
    let arp_target_mac = args.get_one::<String>("arp_target_mac").unwrap();
    let arp_source_mac = args.get_one::<String>("arp_source_mac").unwrap();
    let arp_target_ip = args.get_one::<String>("arp_target_ip").unwrap();
    let arp_source_ip = args.get_one::<String>("arp_source_ip").unwrap();
    let ether_target_mac = args.get_one::<String>("ether_target_mac").unwrap();
    let ether_source_mac = args.get_one::<String>("ether_source_mac").unwrap();
    println!("interface {}", interface);
    println!("arp_target_mac {:#?}", arp_target_mac);
    println!("arp_source_mac {:#?}", arp_source_mac);
    println!("arp_target_ip {:#?}", arp_target_ip);
    println!("arp_source_ip {:#?}", arp_source_ip);
    println!("ether_target_mac {:#?}", ether_target_mac);
    println!("ether_source_mac {:#?}", ether_source_mac);

    let interface = crate::interface::from_string(interface)
        .ok_or(anyhow!("Invalid interface. Interfaces found on your system: {:?}", crate::interface::list_interfaces().iter().map(|i| i.name.as_str()).collect::<Vec<&str>>()))?;
    println!("interface {}", interface);

    let arp_target_mac = pnet::datalink::MacAddr::from_str(arp_target_mac).map_err(|_| anyhow!("Invalid MAC address for arp_target_mac"))?;
    let arp_source_mac = pnet::datalink::MacAddr::from_str(arp_source_mac).map_err(|_| anyhow!("Invalid MAC address for arp_source_mac"))?;
    let ether_target_mac = pnet::datalink::MacAddr::from_str(ether_target_mac).map_err(|_| anyhow!("Invalid MAC address for ether_target_mac"))?;
    let ether_source_mac = pnet::datalink::MacAddr::from_str(ether_source_mac).map_err(|_| anyhow!("Invalid MAC address for ether_source_mac"))?;
    let arp_target_ip = std::net::Ipv4Addr::from_str(arp_target_ip).map_err(|_| anyhow!("Invalid IP address for arp_target_ip"))?;
    let arp_source_ip = std::net::Ipv4Addr::from_str(arp_source_ip).map_err(|_| anyhow!("Invalid IP address for arp_source_ip"))?;
    println!("arp_target_mac {:#?}", arp_target_mac);
    println!("arp_source_mac {:#?}", arp_source_mac);
    println!("arp_target_ip {:#?}", arp_target_ip);
    println!("arp_source_ip {:#?}", arp_source_ip);
    println!("ether_target_mac {:#?}", ether_target_mac);
    println!("ether_source_mac {:#?}", ether_source_mac);

    Ok((
        interface,
        arp_target_mac,
        arp_source_mac,
        arp_target_ip,
        arp_source_ip,
        ether_target_mac,
        ether_source_mac,
    ))
}