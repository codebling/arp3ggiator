// from https://github.com/libpnet/libpnet/blob/05db0f1acbc78d2d76f022a688982069c27246e8/examples/arp_packet.rs 
extern crate pnet;

use std::net::{IpAddr, Ipv4Addr};

use pnet::datalink::{Channel, MacAddr, NetworkInterface, Config, DataLinkReceiver};

use pnet::packet::arp::{ArpHardwareTypes, ArpPacket, MutableArpPacket, ArpOperation};
use pnet::packet::ethernet::EtherTypes;
use pnet::packet::ethernet::MutableEthernetPacket;
use pnet::packet::{MutablePacket, Packet};

pub fn get_mac_through_arp(
    operation: ArpOperation,
    interface: NetworkInterface,
    arp_target_mac: MacAddr,
    arp_source_mac: MacAddr,
    arp_target_ip: Ipv4Addr,
    arp_source_ip: Ipv4Addr,
    ether_target_mac: MacAddr,
    ether_source_mac: MacAddr,
) -> Result<Box<dyn DataLinkReceiver>, anyhow::Error> {
    let source_ip = interface
        .ips
        .iter()
        .find(|ip| ip.is_ipv4())
        .map(|ip| match ip.ip() {
            IpAddr::V4(ip) => ip,
            _ => unreachable!(),
        })
        .unwrap_or(Ipv4Addr::new(0, 0, 0, 0));

    let mut config: Config = Default::default();
    config.promiscuous = false;
    println!("Using interface: {:#?}", interface);
    println!("Using source ip: {:#?}", source_ip);
    println!("Using default config: {:#?}", config);

    let (mut sender, receiver) = match pnet::datalink::channel(&interface, config) {
        Ok(Channel::Ethernet(tx, rx)) => (tx, rx),
        Ok(_) => panic!("Unknown channel type"),
        Err(e) => panic!("Error occurred while opening channel: {:#?}", e),
    };

    let mut ethernet_buffer = [0u8; 42];
    let mut ethernet_packet = MutableEthernetPacket::new(&mut ethernet_buffer).unwrap();

    // let destination = MacAddr::broadcast();
    // let source = interface.mac.unwrap();
    ethernet_packet.set_destination(ether_target_mac);
    ethernet_packet.set_source(ether_source_mac);
    ethernet_packet.set_ethertype(EtherTypes::Arp);

    let mut arp_buffer = [0u8; 28];
    let mut arp_packet = MutableArpPacket::new(&mut arp_buffer).unwrap();

    arp_packet.set_hardware_type(ArpHardwareTypes::Ethernet);
    arp_packet.set_protocol_type(EtherTypes::Ipv4);
    arp_packet.set_hw_addr_len(6);
    arp_packet.set_proto_addr_len(4);
    arp_packet.set_operation(operation);
    arp_packet.set_sender_hw_addr(arp_source_mac);
    arp_packet.set_sender_proto_addr(arp_source_ip);
    arp_packet.set_target_hw_addr(arp_target_mac);
    arp_packet.set_target_proto_addr(arp_target_ip);

    ethernet_packet.set_payload(arp_packet.packet_mut());

    sender
        .send_to(ethernet_packet.packet(), None)
        .unwrap()
        .unwrap();

    println!("Sent ARP request");

    Ok(receiver)
}

fn receive_packet(mut receiver: Box<dyn DataLinkReceiver>) -> Result<ArpPacket<'static>, anyhow::Error> {
    let buf = receiver.next()?;

    let data = Vec::from(buf);
    let arp_packet = ArpPacket::owned(data).unwrap();

    Ok(arp_packet)
}