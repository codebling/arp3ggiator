use pnet::datalink::NetworkInterface;


pub fn list_interfaces() -> Vec<NetworkInterface> {
    pnet::datalink::interfaces()
}

pub fn from_string(interface_name: &str) -> Option<NetworkInterface> {
  list_interfaces()
    .iter()
    .find(|nic| nic.name.as_str() == interface_name)
    .map(|nic| nic.to_owned())
}