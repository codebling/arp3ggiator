# Arp3ggiator

Arp3ggiator is an [ARP](https://en.wikipedia.org/wiki/Address_Resolution_Protocol) test tool. There are already quite a few ARP tools out there but I wasn't able to find one that let me separately specify the ARP payload's source and destination MACs and the ethernet frame/wire layer's source and destination MAC addresses. This tool does can do that. 

# Usage

Usage info can be displayed by running `--help`:
```
arp3ggiator --help
```

You'll choose one mode, "query" (send an ARP request, `-Q`) or "answer" (send an unsolicited ARP response, `-A`) and an interface (`-i` with the name of a link-layer device). Everything else is optional:

```
arp3ggiator 
  -query | -answer
  --interface 
  --arp_target_mac
  --arp_source_mac
  --arp_target_ip
  --arp_source_ip
  --ether_target_mac
  --ether_source_mac
```

# To do:

- [ ] Listen for a response to a query
- [ ] Better handling and feedback when interfaces are down or in a weird state
- [ ] Better defaults, especially for query mode, so that users can send a valid query packet with minimal typing
- [ ] Documentation of specific use-cases, especially when ethernet frame and ARP payload have different source and target MAC addresses
